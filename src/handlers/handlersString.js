export function handlerAddString({
  setArrayString,
  string,
  closeModal,
  setError,
  setString,
}) {
  if (!string) {
    setError(true);
  } else {
    setArrayString((array) => [...array, string]);
    closeModal();
    setString("");
  }
}

export function handlerRemoveString({
  setArrayString,
  position,
  setSelectedItem,
}) {
  setArrayString((array) =>
    array.filter((string, index) => index !== position)
  );

  setSelectedItem && setSelectedItem(null);
}
