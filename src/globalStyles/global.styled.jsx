import styled from "styled-components";

export const MainContainer = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  background: rgb(161, 196, 253);
  background: linear-gradient(
    150deg,
    rgba(161, 196, 253, 1) 35%,
    rgba(194, 233, 251, 1) 100%
  );
`;

export const ButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: ${(props) => (props.contentEnd ? "end" : "space-between")};
  div {
    width: 30%;
    display: flex;
    justify-content: space-between;
  }
`;
