import React, { useState } from "react";

import Button from "../Buttons/Button";
import {
  StringBoxContainer,
  StringBoxTitle,
  StringBoxText,
  StringBoxList,
  StringBoxListItem,
} from "./StringBox.styled";
import { ButtonsContainer } from "../../globalStyles/global.styled";
import { GenericButton } from "../Buttons/Button.styled";
import {
  handlerAddString,
  handlerRemoveString,
} from "../../handlers/handlersString";
import AddStringModal from "../Modal/Modal";

const StringBox = (props) => {
  const [addString, setArrayString] = useState([]);
  const [string, setString] = useState("");
  const [selectedItem, setSelectedItem] = useState(null);
  const [modalIsOpen, setIsOpen] = useState(false);
  const [error, setError] = useState(false);

  const openModal = () => {
    setIsOpen(true);
  };
  const closeModal = () => {
    setIsOpen(false);
    setError(false);
  };

  return (
    <>
      <StringBoxContainer role="logic-component">
        <StringBoxTitle>This is a technical proof</StringBoxTitle>
        <StringBoxText role="text_string">{addString[selectedItem?.target.id]}</StringBoxText>
        <StringBoxList>
          {addString &&
            addString.map((string, index) => {
              return (
                <StringBoxListItem
                  key={index}
                  selected={parseInt(selectedItem?.target.id) === index}
                >
                  <p onClick={(e) => setSelectedItem(e)} id={index}>
                    Item {index + 1}
                  </p>
                </StringBoxListItem>
              );
            })}
        </StringBoxList>
        <ButtonsContainer>
          <div>
            <Button
              setArrayString={setArrayString}
              setSelectedItem={setSelectedItem}
              text={"icon"}
            />
            <Button
              functionHandler={handlerRemoveString}
              setArrayString={setArrayString}
              position={parseInt(selectedItem?.target.id)}
              text={"delete"}
            />
          </div>

          <GenericButton background onClick={openModal}>
            ADD
          </GenericButton>
        </ButtonsContainer>
        <AddStringModal
          modalIsOpen={modalIsOpen}
          closeModal={closeModal}
          handlerAddString={handlerAddString}
          setArrayString={setArrayString}
          string={string}
          setString={setString}
          setError={setError}
          error={error}
        />
      </StringBoxContainer>
    </>
  );
};

export default StringBox;
