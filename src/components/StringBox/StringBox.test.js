import { render, screen } from "@testing-library/react";
import StringBox from "./StringBox";

describe("StrinBox Component", () => {
  const setup = () => render(<StringBox />);
  let deleteBtn;
  let addBtn;
  let refreshBtn;
  beforeEach(() => {
    // eslint-disable-next-line testing-library/no-render-in-setup
    render(<StringBox />);
    deleteBtn = screen.getByRole("button", { name: /delete/i });
    addBtn = screen.getByRole("button", { name: /add/i });
    refreshBtn = screen.getByRole("button", { name: /image-button/i });
  });

  test("should render component", () => {
    // eslint-disable-next-line no-unused-expressions
    expect(setup()).toBeTruthy();
  });

  test("should have all buttons", () => {
    expect(deleteBtn).toBeInTheDocument();
    expect(addBtn).toBeInTheDocument();
    expect(refreshBtn).toBeInTheDocument();
  });
});
