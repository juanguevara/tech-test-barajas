import styled from "styled-components";

export const StringBoxContainer = styled.div`
  display: flex;
  position: relative;
  width: 900px;
  flex-direction: column;
  box-shadow: 0px 5px 12px #0000001f;
  background: #ffffff 0% 0% no-repeat padding-box;
  border-radius: 20px;
  padding: 50px;
  align-items: center;
`;

export const StringBoxTitle = styled.h1`
  text-align: center;
  font-family: "Montserrat", sans-serif;
  font-weight: 500;
  letter-spacing: 0px;
  color: #333333;
  opacity: 1;
`;

export const StringBoxText = styled.p`
  text-align: center;
  font-family: "Montserrat", sans-serif;
  font-size: 18px;
  letter-spacing: 0px;
  color: #333333;
`;

export const StringBoxList = styled.div`
  padding: 5px 0;
  background: #f7f7f7 0% 0% no-repeat padding-box;
  border: 1px solid #cccccc;
  font-family: "Montserrat", sans-serif;
  font-weight: 500;
  opacity: 1;
  width: 100%;
  height: 227px;
  margin: 20px 0;
  overflow: scroll;
  &::-webkit-scrollbar {
    display: none;
  }
`;

export const StringBoxListItem = styled.div`
  padding: 0 15px;
  height: 40px;
  align-items: center;
  display: flex;
  cursor: pointer;
  p {
    margin: 0;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    background: ${(props) => (props.selected ? "#324bff" : "unset")};
    color: ${(props) => (props.selected ? "white" : "unset")};
    &:hover {
      background: #324bff;
      color: white;
    }
  }
`;
