import React from "react";
import PropTypes from "prop-types";

import { Input } from "./TextInput.styled";

const TextInput = ({ setString }) => {
  return (
    <>
      <Input type="text" onChange={(e) => setString(e.target.value)} />
    </>
  );
};

TextInput.propTypes = {
  TextInput: PropTypes.func,
};

export default TextInput;
