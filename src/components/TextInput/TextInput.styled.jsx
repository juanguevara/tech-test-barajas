import styled from "styled-components";

export const Input = styled.input`
  height: 60px;
  width: 580px;
  max-width: 600px;
  background: #f7f7f7 0% 0% no-repeat padding-box;
  border: 1px solid #cccccc;
  font-size: 18px;
  font-family: "Montserrat", sans-serif;
  font-weight: 500;
  padding-left: 15px;
  margin: 20px 0;
`;
