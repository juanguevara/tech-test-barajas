import { render, screen } from "@testing-library/react";
import TextInput from "./TextInput";

describe("TextInput Component", () => {
  test("should render component", () => {
    // eslint-disable-next-line no-unused-expressions
    expect(render(<TextInput />)).toBeTruthy();
  });
});
