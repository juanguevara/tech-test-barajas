import React from "react";
import PropTypes from "prop-types";

import Modal from "react-modal";

import Button from "../Buttons/Button";
import TextInput from "../TextInput/TextInput";
import { GenericButton } from "../Buttons/Button.styled";
import { ModalContainer, ModalTitle, ModalError } from "./Modal.styled";
import { ButtonsContainer } from "../../globalStyles/global.styled";

const customStyles = {
  content: {
    top: "25%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "700px",
    height: "276px",
    borderRadius: "20px",
  },
};

const AddStringModal = ({
  modalIsOpen,
  closeModal,
  handlerAddString,
  setArrayString,
  string,
  setString,
  setError,
  error,
}) => {
  return (
    // eslint-disable-next-line jsx-a11y/aria-role
    <div role="modal">
      <Modal
        style={customStyles}
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel="Add String Modal"
        ariaHideApp={false}
      >
        <ModalContainer>
          <ModalTitle>Add item to list</ModalTitle>
          <TextInput setString={setString}></TextInput>
          {error && <ModalError role="warning">It's empty</ModalError>}
          <ButtonsContainer contentEnd>
            <Button
              functionHandler={handlerAddString}
              setArrayString={setArrayString}
              setString={setString}
              string={string}
              background
              text={"ADD"}
              closeModal={closeModal}
              setError={setError}
            />
            <GenericButton onClick={closeModal} mLeft>
              CLOSE
            </GenericButton>
          </ButtonsContainer>
        </ModalContainer>
      </Modal>
    </div>
  );
};

Modal.propTypes = {
  modalIsOpen: PropTypes.bool,
  closeModal: PropTypes.func,
  handlerAddString: PropTypes.func,
  setArrayString: PropTypes.func,
  setString: PropTypes.func,
  string: PropTypes.string,
  setError: PropTypes.func,
  error: PropTypes.bool,
};

export default AddStringModal;
