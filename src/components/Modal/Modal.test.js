import { render, screen } from "@testing-library/react";
import AddStringModal from "./Modal";

describe("AddStringModal Component", () => {
  test("should render component", () => {
    render(<AddStringModal />);
    // eslint-disable-next-line no-unused-expressions
    expect(render(<AddStringModal />)).toBeTruthy();
  });
  test("should not openmodal", () => {
    render(<AddStringModal modalIsOpen={false} />);
    const modal = screen.getByRole("modal");
    // eslint-disable-next-line testing-library/no-node-access
    expect(modal.children.length).toBe(0);
  });

  test("should show error", () => {
    render(<AddStringModal error={true} modalIsOpen={true} />);
    const error = screen.getByText(/It's empty/i);
    expect(error).toBeInTheDocument();
  });
});
