import styled from "styled-components";

export const ModalContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  justify-content: center;
  height: 100%;
  padding: 0 50px;
`;

export const ModalTitle = styled.h1`
  font-size: 18px;
  font-family: "Montserrat", sans-serif;
  font-weight: 500;
  letter-spacing: 0px;
  color: #333333;
  width: 100%;
  margin-bottom: 0;
`;

export const ModalError = styled.p`
    color:red;
`;
