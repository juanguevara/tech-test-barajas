import React from "react";
import PropTypes from "prop-types";

import { GenericButton } from "./Button.styled";
import icon from "./../../assets/icon/refresh.svg";

const Button = ({
  functionHandler,
  setArrayString,
  setString,
  setSelectedItem,
  string,
  position,
  background,
  text,
  closeModal,
  setError,
}) => {
  return (
    <>
      <GenericButton
        background={background}
        onClick={() => {
          if (functionHandler) {
            functionHandler({
              setArrayString,
              setString,
              string,
              position,
              setSelectedItem,
              closeModal,
              setError,
            });
          } else {
            setArrayString && setArrayString([]);
            setSelectedItem && setSelectedItem(null);
          }
        }}
        aria-label={text === "icon" ? "image-button" : ""}
      >
        {text === "icon" ? <img src={icon} alt="refresh" /> : <p>{text}</p>}
      </GenericButton>
    </>
  );
};

Button.propTypes = {
  functionHandler: PropTypes.func,
  setArrayString: PropTypes.func,
  setSelectedItem: PropTypes.func,
  setString: PropTypes.func,
  string: PropTypes.string,
  position: PropTypes.number,
  text: PropTypes.string,
  closeModal: PropTypes.func,
  setError: PropTypes.func,
  background: PropTypes.bool,
};

export default Button;
