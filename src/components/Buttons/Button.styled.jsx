import styled from "styled-components";

export const GenericButton = styled.button`
  border: 1px solid #324bff;
  border-radius: 50px;
  opacity: 1;
  height: 49px;
  background: ${(props) => (props.background ? "#324BFF" : "white")};
  color: ${(props) => (props.background ? "white" : "#324bff")};
  cursor: pointer;
  padding: 15px 50px;
  position: relative;
  text-transform: uppercase;
  margin-left: ${(props) => (props.mLeft ? "15px" : "unset")};
  &:hover {
    background: #324bff;
    color: white;
    img {
      filter: invert(100%) sepia(96%) saturate(15%) hue-rotate(212deg)
        brightness(104%) contrast(104%);
    }
  }
  p {
    font-family: "Montserrat", sans-serif;
    font-weight: 500;
    margin: 0;
  }
  img {
    width: 25px;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    filter: invert(24%) sepia(95%) saturate(3489%) hue-rotate(232deg)
      brightness(98%) contrast(105%);
  }
`;
