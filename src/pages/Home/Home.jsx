import React from "react";

import StringBox from "../../components/StringBox/StringBox";
import { MainContainer } from "../../globalStyles/global.styled";

const Home = (props) => {
  return (
    <MainContainer>
      <StringBox/>
    </MainContainer>
  );
};

export default Home;
