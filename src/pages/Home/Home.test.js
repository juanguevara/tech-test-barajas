import { render, screen } from "@testing-library/react";
import Home from "./Home";

describe("Home Page", () => {
  test("should render component", () => {
    render(<Home />);
    expect(render(<Home />)).toBeTruthy();
  });

  test("should have StringBox component", () => {
    render(<Home />);
    expect(screen.getByRole("logic-component")).toBeInTheDocument();
  });
});
