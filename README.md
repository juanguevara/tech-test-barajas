# Techincal-Proof

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
The design and funcionalitys of this proof come from this [Adobe XD Page](https://xd.adobe.com/view/ea696dd0-8781-4460-8720-36deb2d19b2a-bf3a/)


### To run proof `npm install & npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### Tools used in the proof
React Hooks
Styled-Components
React-Testing-Library


### Things i couldn't finish for lack of time
Complete all tests for components

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


